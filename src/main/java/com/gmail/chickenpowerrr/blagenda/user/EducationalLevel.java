package com.gmail.chickenpowerrr.blagenda.user;

public enum EducationalLevel {
    VWO,
    HAVO;
}
