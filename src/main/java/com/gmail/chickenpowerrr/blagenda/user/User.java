package com.gmail.chickenpowerrr.blagenda.user;

import com.gmail.chickenpowerrr.blagenda.form.RegisterForm;
import com.gmail.chickenpowerrr.blagenda.validator.UniqueUsername;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.xml.bind.DatatypeConverter;
import lombok.Data;

@Data
@Entity
public class User {

  private static final MessageDigest digest;

  static {
    try {
      digest = MessageDigest.getInstance("SHA-256");
    } catch (NoSuchAlgorithmException e) {
      throw new RuntimeException(e);
    }
  }

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private long id;

  @Column
  private EducationalLevel educationalLevel;
  @Column
  private int grade;
  @Column(unique = true)
  private String username;
  @Column
  private String password;

  public User(RegisterForm registerForm) {
    this(registerForm.getEducationalLevel(), registerForm.getGrade(), registerForm.getUsername(),
        registerForm.getPassword());
  }

  public User(EducationalLevel educationalLevel, int grade, @UniqueUsername String username,
      String password) {
    this.educationalLevel = educationalLevel;
    this.grade = grade;
    this.username = username;
    this.password = hash(password);
  }

  protected User() {

  }

  public void updatePassword(String password) {
    this.password = hash(password);
  }

  private String hash(String input) {
    byte[] hash = digest.digest(input.getBytes());
    return DatatypeConverter.printHexBinary(hash);
  }
}
