package com.gmail.chickenpowerrr.blagenda.calendar;

import com.gmail.chickenpowerrr.blagenda.user.EducationalLevel;
import com.gmail.chickenpowerrr.blagenda.user.User;
import com.google.api.client.util.DateTime;
import com.google.api.services.calendar.model.Event;
import com.google.api.services.calendar.model.EventDateTime;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import lombok.ToString;
import org.springframework.web.client.RestTemplate;

@ToString
public class CalendarEvent {

  private static final Pattern titleHtmlPattern = Pattern.compile("<title>.+?</title>");
  private static final Pattern descriptionPattern = Pattern
      .compile("DESCRIPTION.+ATTACH;", Pattern.DOTALL);
  private static final Pattern startTimePattern = Pattern.compile("DTSTART;[^ \n]+");
  private static final Pattern endTimePattern = Pattern.compile("DTEND;[^ \n]+");
  private static final Pattern numberPattern = Pattern.compile("[0-9]+");

  private final String name;
  private final String description;
  private final String url;
  private final Date startTime;
  private final Date endTime;
  private final Map<EducationalLevel, Collection<Integer>> invited = new HashMap<>();

  public CalendarEvent(String url) {
    RestTemplate restTemplate = new RestTemplate();

    String html = restTemplate.getForObject(url, String.class);
    Matcher titleHtmlMatcher = titleHtmlPattern.matcher(html);
    titleHtmlMatcher.find();

    String ical = restTemplate.getForObject(url + "ical/", String.class);
    Matcher startTimeMatcher = startTimePattern.matcher(ical);
    startTimeMatcher.find();
    toTime(startTimeMatcher.group());
    Matcher endTimeMatcher = endTimePattern.matcher(ical);
    endTimeMatcher.find();

    Matcher descriptionMatcher = descriptionPattern.matcher(ical);
    if (descriptionMatcher.find()) {
      this.description = descriptionMatcher.group().replace("DESCRIPTION:", "").replace("\\,", ",")
          .replaceAll("\\s{2,}+", "").replace("\\n", "\n")
          .replaceAll("\\s*ATTACH;", "");
    } else {
      this.description = null;
    }

    this.url = url;
    this.name = titleHtmlMatcher.group().replace("<title>", "").replace("</title>", "");
    this.startTime = toTime(startTimeMatcher.group());
    this.endTime = toTime(endTimeMatcher.group());

    updateTarget();
  }

  public boolean isInvited(User user) {
    if (!this.invited.isEmpty()) {
      if (this.invited.containsKey(user.getEducationalLevel())) {
        Collection<Integer> invitedGrades = this.invited.get(user.getEducationalLevel());
        if (!invitedGrades.isEmpty()) {
          return invitedGrades.contains(user.getGrade());
        } else {
          return true;
        }
      } else {
        return false;
      }
    } else {
      return true;
    }
  }

  private Date toTime(String dateString) {
    Matcher numberMatcher = numberPattern.matcher(dateString);

    String date, time;
    date = time = null;

    while (numberMatcher.find()) {
      if (date == null) {
        date = numberMatcher.group();
      } else {
        time = numberMatcher.group();
      }
    }

    Calendar.Builder calendarBuilder = new Calendar.Builder();
    int year = Integer.parseInt(date.substring(0, 4));
    int month = Integer.parseInt(date.substring(4, 6)) - 1;
    int day = Integer.parseInt(date.substring(6, 8));

    calendarBuilder.setDate(year, month, day);

    if (time != null) {
      int hour = Integer.parseInt(time.substring(0, 2));
      int minute = Integer.parseInt(time.substring(2, 4));
      int second = Integer.parseInt(time.substring(4, 6));
      calendarBuilder.setTimeOfDay(hour, minute, second);
    }

    return calendarBuilder.build().getTime();
  }

  private Collection<Integer> getGrades(EducationalLevel educationalLevel) {
    if (this.description.toLowerCase().contains(educationalLevel.name().toLowerCase())) {
      return new HashSet<>();
    } else {
      return null;
    }
  }

  private void updateTarget() {
    for (EducationalLevel educationalLevel : EducationalLevel.values()) {
      Collection<Integer> invitedGrades = getGrades(educationalLevel);

      if (invitedGrades != null) {
        this.invited.put(educationalLevel, invitedGrades);
      }
    }
  }

  public Event toGoogleEvent() {
    Event event = new Event();
    if (this.startTime != null) {
      event.setStart(toEventDateTime(this.startTime));
    }

    if (this.endTime != null) {
      event.setEnd(toEventDateTime(this.endTime));
    }

    event.setSummary(this.name);
    event.setDescription(this.description == null ? this.url : this.description);
    return event;
  }

  private EventDateTime toEventDateTime(Date date) {
    return new EventDateTime().setDateTime(new DateTime(date)).setTimeZone("Europe/Amsterdam");
  }
}
