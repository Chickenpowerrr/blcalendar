package com.gmail.chickenpowerrr.blagenda.calendar;

import com.gmail.chickenpowerrr.blagenda.user.User;
import com.google.api.services.calendar.model.Event;
import java.util.Collection;
import java.util.HashSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

public class BLCalendarRetriever {

  private static final String url = "https://www.bataafslyceum.nl/agenda/page/%s/";
  private static final Pattern agendaHtmlPattern = Pattern
      .compile("<a href=\"https://www.bataafslyceum.nl/.+?/\" class=\"calendar__item\">");
  private static final Pattern agendaUrlPattern = Pattern
      .compile("https://www\\.bataafslyceum\\.nl/agenda/.+?/");

  private final Collection<CalendarEvent> calendarEvents;

  public BLCalendarRetriever() {
    this.calendarEvents = toAgendaEvents(getAgendaUrls());
  }

  public Collection<Event> getEvents(User user) {
    return this.calendarEvents.stream().filter(calendarEvent -> calendarEvent.isInvited(user))
        .map(CalendarEvent::toGoogleEvent).collect(Collectors.toSet());
  }

  private Collection<CalendarEvent> toAgendaEvents(Collection<String> agendaUrls) {
    return agendaUrls.stream().map(CalendarEvent::new).collect(Collectors.toSet());
  }

  private Collection<String> getAgendaUrls() {
    Collection<String> urls = new HashSet<>();

    try {
      for (int i = 1; ; i++) {
        RestTemplate restTemplate = new RestTemplate();
        String html = restTemplate.getForObject(String.format(url, i), String.class);
        if (html != null) {
          Matcher agendaHtmlMatcher = agendaHtmlPattern.matcher(html);

          while (agendaHtmlMatcher.find()) {
            String htmlTag = agendaHtmlMatcher.group();

            Matcher agendaUrlMatcher = agendaUrlPattern.matcher(htmlTag);
            if (agendaUrlMatcher.find()) {
              urls.add(agendaUrlMatcher.group());
            }
          }
        }
      }
    } catch (HttpClientErrorException e) {

    }
    return urls;
  }
}
