package com.gmail.chickenpowerrr.blagenda.calendar;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class CalendarConfig {

  @Bean
  public BLCalendarRetriever calendarRetriever() {
    return new BLCalendarRetriever();
  }
}
