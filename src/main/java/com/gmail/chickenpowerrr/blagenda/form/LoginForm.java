package com.gmail.chickenpowerrr.blagenda.form;

import javax.validation.constraints.Size;
import lombok.Data;

@Data
public class LoginForm {

  @Size(min = 3, max = 40)
  private String username;
  @Size(min = 4, max = 100)
  private String password;
}
