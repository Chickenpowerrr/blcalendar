package com.gmail.chickenpowerrr.blagenda.form;

import com.gmail.chickenpowerrr.blagenda.user.EducationalLevel;
import com.gmail.chickenpowerrr.blagenda.validator.UniqueUsername;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import lombok.Data;

@Data
public class RegisterForm {

  @Size(min = 3, max = 50)
  @UniqueUsername
  private String username;
  @Size(min = 4, max = 100)
  private String password;
  private int grade;
  @NotNull
  private EducationalLevel educationalLevel;
}
