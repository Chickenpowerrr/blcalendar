package com.gmail.chickenpowerrr.blagenda.controller;

import com.gmail.chickenpowerrr.blagenda.calendar.BLCalendarRetriever;
import com.gmail.chickenpowerrr.blagenda.user.User;
import com.google.api.client.googleapis.batch.BatchRequest;
import com.google.api.client.googleapis.batch.json.JsonBatchCallback;
import com.google.api.client.googleapis.json.GoogleJsonError;
import com.google.api.client.http.HttpHeaders;
import com.google.api.services.calendar.Calendar;
import com.google.api.services.calendar.model.Event;
import com.google.api.services.calendar.model.Events;
import java.io.IOException;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class FinishedController {

  private BLCalendarRetriever calendarRetriever;
  private final JsonBatchCallback<Event> batchCallback = new JsonBatchCallback<>() {

    public void onSuccess(Event event, HttpHeaders responseHeaders) {

    }

    public void onFailure(GoogleJsonError e, HttpHeaders responseHeaders) {
      System.out.println("Error Message: " + e.getMessage());
    }
  };

  @Autowired
  public FinishedController(BLCalendarRetriever calendarRetriever) {
    this.calendarRetriever = calendarRetriever;
  }

  @GetMapping(value = "/finished")
  public String finished(Model model, HttpServletRequest request) throws IOException {
    User user = ((User) request.getSession().getAttribute("user"));
    Calendar calendar = ((Calendar) request.getSession().getAttribute("Google"));

    if (calendar != null) {
      BatchRequest batch = calendar.batch();
      Map<String, Collection<Event>> events = getEvents(calendar);

      for (Event event : this.calendarRetriever.getEvents(user)) {
        if (!(events.containsKey(event.getSummary()) && events.get(event.getSummary()).stream()
            .anyMatch(e -> e.getStart().equals(event.getStart())))) {
          calendar.events().insert("primary", event).queue(batch, this.batchCallback);
        }
      }
      if (batch.size() > 0) {
        batch.execute();
      }
    }

    if (user != null) {
      model.addAttribute("user", ((User) request.getSession().getAttribute("user")).getUsername());
      return "/finished";
    } else {
      return "redirect:/index";
    }
  }

  private Map<String, Collection<Event>> getEvents(Calendar calendar) throws IOException {
    Map<String, Collection<Event>> sortedEvents = new HashMap<>();

    String pageToken = null;

    do {
      Events events = calendar.events().list("primary").setPageToken(pageToken).execute();
      List<Event> items = events.getItems();
      for (Event event : items) {
        if (!sortedEvents.containsKey(event.getSummary())) {
          sortedEvents.put(event.getSummary(), new HashSet<>());
        }
        sortedEvents.get(event.getSummary()).add(event);
      }
      pageToken = events.getNextPageToken();
    } while (pageToken != null);

    return sortedEvents;
  }
}
