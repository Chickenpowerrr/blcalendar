package com.gmail.chickenpowerrr.blagenda.controller;

import com.gmail.chickenpowerrr.blagenda.form.LoginForm;
import com.gmail.chickenpowerrr.blagenda.repository.UserRepository;
import com.gmail.chickenpowerrr.blagenda.user.User;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import javax.xml.bind.DatatypeConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.view.RedirectView;

@Controller
public class LoginController {

  private static final MessageDigest digest;

  static {
    try {
      digest = MessageDigest.getInstance("SHA-256");
    } catch (NoSuchAlgorithmException e) {
      throw new RuntimeException(e);
    }
  }

  private final UserRepository userRepository;

  @Autowired
  public LoginController(UserRepository userRepository) {
    this.userRepository = userRepository;
  }

  @GetMapping(value = "/login")
  public String login(@ModelAttribute("form") LoginForm loginForm) {
    return "login";
  }

  @PostMapping(value = "/login")
  public RedirectView loginSubmit(@Valid @ModelAttribute("form") LoginForm loginForm,
      BindingResult bindingResult, Model model, HttpServletRequest request) {
    if (bindingResult.hasErrors()) {
      return null;
    }

    User user = this.userRepository.findUserByUsername(loginForm.getUsername());

    if (user != null && user.getPassword().equals(hash(loginForm.getPassword()))) {
      request.getSession().setAttribute("user", user);
      return new RedirectView("/login/google/");
    } else {
      return null;
    }
  }

  private String hash(String input) {
    byte[] hash = digest.digest(input.getBytes());
    return DatatypeConverter.printHexBinary(hash);
  }
}
