package com.gmail.chickenpowerrr.blagenda.controller;

import com.gmail.chickenpowerrr.blagenda.form.RegisterForm;
import com.gmail.chickenpowerrr.blagenda.repository.UserRepository;
import com.gmail.chickenpowerrr.blagenda.user.EducationalLevel;
import com.gmail.chickenpowerrr.blagenda.user.User;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.view.RedirectView;

@Controller
public class RegisterController {

  private final UserRepository userRepository;

  @Autowired
  public RegisterController(UserRepository userRepository) {
    this.userRepository = userRepository;
  }

  @GetMapping(value = "/register")
  public String register(@ModelAttribute("form") RegisterForm registerForm, Model model,
      HttpServletRequest request) {
    if (request.getSession().getAttribute("user") == null) {
      model.addAttribute("educationalLevels", EducationalLevel.values());
      return "/register";
    } else {
      return "redirect:/login";
    }
  }

  @PostMapping(value = "/register")
  public RedirectView registerSubmit(@Valid @ModelAttribute("form") RegisterForm registerForm,
      BindingResult bindingResult, Model model, HttpServletRequest request) {
    if (request.getSession().getAttribute("user") == null) {
      if (bindingResult.hasErrors()) {
        model.addAttribute("educationalLevels", EducationalLevel.values());
        return null;
      }

      User user = new User(registerForm);

      this.userRepository.save(user);
      request.getSession().setAttribute("user", user);
      return new RedirectView("/login");
    } else {
      return new RedirectView("/index");
    }
  }
}
