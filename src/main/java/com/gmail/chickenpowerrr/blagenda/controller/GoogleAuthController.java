package com.gmail.chickenpowerrr.blagenda.controller;

import com.google.api.client.auth.oauth2.AuthorizationCodeRequestUrl;
import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.auth.oauth2.TokenResponse;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeFlow;
import com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets;
import com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets.Details;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.calendar.Calendar;
import com.google.api.services.calendar.CalendarScopes;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.Collections;
import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.view.RedirectView;

@Controller
public class GoogleAuthController {

  private static final JsonFactory JSON_FACTORY;
  private static final HttpTransport HTTP_TRANSPORT;

  static {
    JSON_FACTORY = JacksonFactory.getDefaultInstance();
    try {
      HTTP_TRANSPORT = GoogleNetHttpTransport.newTrustedTransport();
    } catch (GeneralSecurityException | IOException e) {
      throw new RuntimeException(e);
    }
  }

  private GoogleAuthorizationCodeFlow flow;

  @Value("${google.client.application-name}")
  private String applicationName;
  @Value("${google.client.client-id}")
  private String clientId;
  @Value("${google.client.client-secret}")
  private String clientSecret;
  @Value("${google.client.redirect-uri}")
  private String redirectURI;

  @PostConstruct
  public void setupFlow() {
    Details web = new Details();
    web.setClientId(this.clientId);
    web.setClientSecret(this.clientSecret);
    GoogleClientSecrets clientSecrets = new GoogleClientSecrets().setWeb(web);
    this.flow = new GoogleAuthorizationCodeFlow.Builder(HTTP_TRANSPORT, JSON_FACTORY, clientSecrets,
        Collections.singleton(CalendarScopes.CALENDAR)).build();
  }

  @GetMapping(value = "/login/google")
  public RedirectView googleLoginAuthenticate(HttpServletRequest request) {
    if (request.getSession().getAttribute("user") != null) {
      AuthorizationCodeRequestUrl authorizationUrl;
      authorizationUrl = this.flow.newAuthorizationUrl().setRedirectUri(this.redirectURI);
      return new RedirectView(authorizationUrl.build());
    } else {
      return new RedirectView("/index");
    }
  }

  @GetMapping(value = "/login/google", params = "code")
  public RedirectView googleLoginCallback(@RequestParam(value = "code") String code,
      HttpServletRequest request) throws IOException {
    if (request.getSession().getAttribute("user") != null) {
      TokenResponse response = this.flow.newTokenRequest(code).setRedirectUri(this.redirectURI)
          .execute();

      if (response != null && !response.isEmpty()) {
        Credential credential = this.flow.createAndStoreCredential(response, "userID");
        Calendar calendar = new Calendar.Builder(HTTP_TRANSPORT, JSON_FACTORY, credential)
            .setApplicationName(this.applicationName).build();
        request.getSession().setAttribute("Google", calendar);
        return new RedirectView("/finished");
      }
    }
    return new RedirectView("redirect:/index");
  }
}
