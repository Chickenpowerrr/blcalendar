package com.gmail.chickenpowerrr.blagenda;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BlCalendarApplication {

  public static void main(String[] args) {
    SpringApplication.run(BlCalendarApplication.class, args);
  }
}
