package com.gmail.chickenpowerrr.blagenda.repository;

import com.gmail.chickenpowerrr.blagenda.user.User;
import org.springframework.data.repository.CrudRepository;

public interface UserRepository extends CrudRepository<User, Long> {

  User findUserByUsername(String username);
  User findUserById(long id);
  boolean existsUserByUsername(String username);
}
