package com.gmail.chickenpowerrr.blagenda.validator;

import com.gmail.chickenpowerrr.blagenda.repository.UserRepository;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import org.springframework.beans.factory.annotation.Autowired;

public class UniqueValidator implements ConstraintValidator<UniqueUsername, String> {

  private UserRepository userRepository;

  @Autowired
  public UniqueValidator(UserRepository userRepository) {
    this.userRepository = userRepository;
  }

  @Override
  public boolean isValid(String username, ConstraintValidatorContext constraintValidatorContext) {
    return username != null && !this.userRepository.existsUserByUsername(username);
  }
}
