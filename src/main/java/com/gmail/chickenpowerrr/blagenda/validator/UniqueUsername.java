package com.gmail.chickenpowerrr.blagenda.validator;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import javax.validation.Constraint;
import javax.validation.Payload;

@Constraint(validatedBy = UniqueValidator.class)
@Retention(RetentionPolicy.RUNTIME)
@Target({ ElementType.PARAMETER, ElementType.FIELD })
public @interface UniqueUsername {

  String message() default "Deze naam is al in gebruik";

  Class<?>[] groups() default {};

  Class<? extends Payload>[] payload() default{};
}
